
const number = Number(prompt("Please input number"));
console.log("your input: " + number);

for (let x = number; x >= 0; x--) {
  if (x <= 50) {
    console.log(
      "The current value is less than or equal to 50."
    );
    break;
  }

  if (x % 10 === 0) {
    console.log("The number is divisible by 10.");
    continue;
  }

  if (x % 5 === 0) {
    console.log(x);
    continue;
  }
}



const word = "supercalifragilisticexpialidocious";
let consonants = "";
console.log(word);
for (let x = 0; x < word.length; x++) {
  if (
    word[x].toLowerCase() == "a" ||
    word[x].toLowerCase() == "e" ||
    word[x].toLowerCase() == "i" ||
    word[x].toLowerCase() == "o" ||
    word[x].toLowerCase() == "u"
  ) {
    continue;
  } else {
    consonants += word[x];
  }
}

console.log("The consonants: " consonants);

